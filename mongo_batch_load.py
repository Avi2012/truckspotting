from truckspotting.models import FoodTruck
from urllib2 import urlopen
import json


def convert_keys_to_string(dictionary):
    """Recursively converts dictionary keys to strings."""
    if not isinstance(dictionary, dict):
        return dictionary
    return dict((str(k), convert_keys_to_string(v))
                for k, v in dictionary.items())


def batch_insert():
    """Batch load data in mongo for food trucks from san fran
    gov data only for trucks with address, longitude and fooditems
    """
    if FoodTruck.objects().count() < 1:
        data_url = urlopen("http://data.sfgov.org/resource/rqzj-sfat.json")
        trucks_json = json.loads(data_url.read())

        trucks_str = [convert_keys_to_string(v) for v in trucks_json]

        for truck in trucks_str:
            if not truck.get("address") or not truck.get("longitude") or not truck.get("fooditems"):
                continue
            truck_entry = FoodTruck(
                address=truck.get("address"),
                applicant=truck.get("applicant", ""),
                fooditems=truck.get("fooditems"),
                status=truck.get("status", ""),
                locationdescription=truck.get("locationdescription", ""),
                objectid=truck.get("objectid", ""),
                location=[float(str(truck.get("longitude"))),
                          float(str(truck.get("latitude")))])
            truck_entry.save()
