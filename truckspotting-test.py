from truckspotting import app
import unittest
import os


class BasicTestCase(unittest.TestCase):

    """Basic App Test Cases"""

    def test_index(self):
        """Ensure flask set up correctly"""
        tester = app.test_client(self)
        response = tester.get('/', content_type='html/text')
        self.assertEqual(response.status_code, 200)

    def test_database(self):
        """Ensure database exists"""
        tester = os.path.exists(os.environ.get('DB_NAME', 'truckspotting'))
        self.assertTrue(tester)

if __name__ == '__main__':
    unittest.main()
