Truckspotting - Find Nearby Foodtrucks
====

[Truckspotting](http://truckspotting-sf.herokuapp.com/) lets you search for your location and displays on a map the food trucks within 2.5 km of where you are. The data comes from [DataSF](http://www.datasf.org/): [Food
Trucks](https://data.sfgov.org/Permitting/Mobile-Food-Facility-Permit/rqzj-sfat). The primary technologies used are

- Python 2.7+
- Flask
- MongoDB and MongoEngine
- Google Maps V3 and Geolocation API
- JavaScript and JQuery
- Bootstrap 3

## Development Environment Setup

Follow these steps to locally deploy the application.

It is strongly recommended that you set up a [virtualenv](http://www.virtualenv.org/) for this project, and you may also want to check out [virtualenvwrapper](http://virtualenvwrapper.readthedocs.org/) for convenience. It is used to sandbox the app from the rest of local environment.

1. Clone the repo locally

	```sh
	$ git clone git@bitbucket.org:Avi2012/truckspotting.git
	```

2. Ensure database is working

	Please follow [installation instructions](http://docs.mongodb.org/manual/installation/) to set up mongodb, then start it with sudo mongod. By default the app creates a database called ``truckspotting`` but you can override it by setting the environment variable DB_NAME.
3. Install application dependencies

	```sh
	$ pip install -r requirements.txt
	```

4. Run the application

	```sh
	$ python manage.py runserver
	```

	Navigate to http://localhost:5000/ in your browser.

## Deploy to Heroku

1. Install the [heroku toolbelt](https://devcenter.heroku.com/articles/quickstart)
2. Follow the instructions to create a new [Python Heroku application](https://devcenter.heroku.com/articles/getting-started-with-python#deploy-your-application-to-heroku)
3. Add the default tier of mongolab to the heroku application

	```sh
	$ heroku addons:add mongolab
	```

4. Ensure that data is loaded

	```sh
	$ heroku run python manage.py shell
	\>\>\> from truckspotting.models import FoodTruck
	\>\>\> FoodTruck.objects().count()
	606
	\>\>\> exit()
	```

	If experiencing issues with loading data, use MongoLab admin tools for debugging

	```sh
	$ heroku addons:open mongolab
	```

5. Run application locally with foreman in the root folder to check it is working

	```sh
	$ foreman start
	```

6. Now navigate to the heroku url provided to check the deployed application. It should be similar to http://truckspotting-sf.herokuapp.com/

### PEP8

All code follows the [PEP 8](http://www.python.org/dev/peps/pep-0008/) style guide, with the exception of E501, E261, and E302.

## Uber Coding Challenge

This application was developed for the Uber Food Trucks challenge, taking on the full-stack track with a relatively stronger back end focus.

### Architechture
Flask was used as the backend web framework for this application due to strong preference of Python, easy decoupling of application components and low boilerplate. A [REST API](http://truckspotting-sf.herokuapp.com/api/v1.0/foodtrucks?longitude=-122.398235074239&latitude=37.7863198120587) was built to get approved food trucks within 2.5km of given latitude and longitude using the geoSphere feature of Mongo. MongoDB was used in the backend since the data is primarily JSON, does not need much normalization or joins within the scope of this application and past experience. MongoEngine was used as an ORM wrapper around Mongo. I have worked with Mongo before but not with geo queries.

The Bootstrap framework is used for rapid prototyping a responsive skeleton for the app. The frontend heavily relies on Google maps apis, using the geocoding APIs to convert the address entered by the user to latitude and longitude and display markers on the map with the trucks data provided by the truckspotting API. I am proficient in both JavaScript and JQuery but I do have more backend experience. This is my first time using the Google Maps for a web app.

### Trade-offs/Left out/Roadblocks

If I had more time, I would try to have MVC on the frontend, perhaps by using a framework such as Backbone, which I do not have much experience with. The API could include the full on CRUD functionality to add, delete, update trucks. The frontend could be customized more with CSS, the info window could potentially include ratings, reviews of trucks using Google/Yelp APIs. Jinja for server side templating could be used if the app were to flesh out into multiple page application with shared components. The user's preference and popularity could be used to rank the trucks that are displayed when there are too many trucks to display. The schedule, provided as pdf, could be parsed with pdfminer or similar pdf parsing tool to only show trucks that are operating when the user is looking.

Due to overcrowded map, first fifty results are returned currently, which as well as the 2.5km distance limit should be subject to iteration.

One primary roadblock was the discovery that the PyPI version of MongoEngine was out of date which meant that its way of doing geoSphere queries is out of date with Mongo 2.6+. I filed an [issue](https://github.com/MongoEngine/mongoengine/issues/736) to reflect that. I included mongoengine as a local dependency due to this reason.

## Other codebases
- [PayPal Python SDK](https://github.com/paypal/rest-api-sdk-python)

## Public Profile
- [Github](https://github.com/avidas)
- [Personal Website](http://aviadas.com/)
- [LinkedIn](https://www.linkedin.com/nhome/)
- [Twitter](https://twitter.com/avidas)