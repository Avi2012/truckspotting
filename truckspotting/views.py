from flask import Blueprint, render_template, jsonify, request, send_from_directory, redirect, url_for
from truckspotting.models import FoodTruck
import os

foodtrucks = Blueprint('foodtrucks', __name__, template_folder='templates')


@foodtrucks.route('/api/v1.0/foodtrucks', methods=['GET'])
def get_trucks():
    longitude = float(request.args.get('longitude', ''))
    latitude = float(request.args.get('latitude', ''))
    # Only get trucks with approved status within 1 km of location and cap at
    # fifty results
    trucks = FoodTruck.objects(status="APPROVED", location__near=[
                               longitude, latitude], location__max_distance=2500)[:50]
    return jsonify({'trucks': trucks})


@foodtrucks.route('/')
def index():
    return render_template('index.html')


@foodtrucks.route('/icon.ico')
def favicon():
    return send_from_directory(os.path.join(foodtrucks.root_path, 'static', 'images'),
                               'icon.ico', mimetype='image/x-icon')


# Simply redirect to home for any other route
@foodtrucks.app_errorhandler(404)
def page_not_found(error):
    return redirect(url_for("foodtrucks.index"), code=302)
