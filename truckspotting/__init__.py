from flask import Flask
import urlparse
from flask.ext.mongoengine import MongoEngine
import os

# If mongolab uri is defined get mongo settings from 
# there or use local setings
app = Flask(__name__)

MONGOLAB_URL = os.environ.get('MONGOLAB_URI')
if MONGOLAB_URL:
    url = urlparse.urlparse(MONGOLAB_URL)
    app.config['MONGODB_SETTINGS'] = {
        'USERNAME': url.username,
        'PASSWORD': url.password,
        'HOST': url.hostname,
        'PORT': url.port,
        'DB': url.path[1:]
    }
else:
    app.config['MONGODB_SETTINGS'] = {
        'DB': os.environ.get('DB_NAME', 'truckspotting')
    }
app.config['SECRET_KEY'] = os.environ.get('SECRET_KEY', 'secret')

# connect MongoEngine with Flask App
db = MongoEngine(app)


def register_blueprints(app):
    # Prevents circular imports
    from truckspotting.views import foodtrucks
    app.register_blueprint(foodtrucks)

register_blueprints(app)
