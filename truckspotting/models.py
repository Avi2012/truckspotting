from truckspotting import db
from datetime import datetime


class FoodTruck(db.DynamicDocument):

    """Truck model, extending dynamic document as
    might decide to add reviews, photos later
    """
    created_at = db.DateTimeField(default=datetime.now, required=True)
    status = db.StringField(max_length=127)
    locationdescription = db.StringField(max_length=512)
    schedule = db.StringField(max_length=512)
    objectid = db.StringField(max_length=127)
    address = db.StringField(required=True, max_length=511)
    applicant = db.StringField(required=True, max_length=255)
    fooditems = db.StringField(required=True, max_length=1027)
    location = db.PointField(required=True)
