// Logic for searching for location, and displaying food trucks
// near the location

$(document).ready(function(){
    google.maps.event.addDomListener(window, 'load', initialize);

    var map;
    var markers = [];
    //Array where each element has a google map latitude, longitude
    //food truck applicant, fooditems and address
    var truckMarkers = [];

    //get truck data from api and populate truckMarkers array with marker data
    function showTrucks(lon, lat){
        $.get("/api/v1.0/foodtrucks", { longitude: lon, latitude: lat }, function (data) {

            //api provides data in a key called trucks
            trucks = data["trucks"];
            for (var i = 0; i < trucks.length; i++) {
                //create new google maps latlng with truck 
                var truckLatLng = new google.maps.LatLng(trucks[i]["location"]["coordinates"][1], trucks[i]["location"]["coordinates"][0]);
                addTruckMarker([truckLatLng, trucks[i]["fooditems"], trucks[i]["applicant"], trucks[i]["address"]]);
            }
            showTruckMarkers();
        })
    }
    
    //Add truck marker, format info window
    // and set event handler to display window 
    // on mouseover and close on mouseout
    function addTruckMarker(location_info) {
        var marker = new google.maps.Marker({
            position: location_info[0],
            map: map,
            title: location_info[2]
        })

        marker.setIcon('http://i.imgur.com/Am7tP4w.png?1');
        location_info.push(marker);

        //Format and initialize info window
        var contentString = '<div id="content">' + '<h5 id="firstHeading" class="firstHeading">' + location_info[2] + '</h5>' +
        '<div id="bodyContent"><p>' + location_info[1] + '</p><p> Address : <b>' + location_info[3] + '</b></p></div></div>';
        var infowindow = new google.maps.InfoWindow({
            content: contentString,
            maxwidth: 300
        });

        //Display info window on hover
        google.maps.event.addListener(marker, 'mouseover', function() {
            infowindow.open(marker.get('map'), marker);
        });

        google.maps.event.addListener(marker, 'mouseout', function() {
            infowindow.close();
        });

        truckMarkers.push(location_info);
    }

    // Add a marker to the map and push to the array.
    function addMarker(location) {
        var marker = new google.maps.Marker({
            position: location,
            map: map,
            animation: google.maps.Animation.DROP
        });
        markers.push(marker);
    }
    
    function initialize() {

        var initialLon = -122.4022724
        var initialLat = 37.7861177
        var initialLatLon = new google.maps.LatLng(initialLat, initialLon);

        //Set up google map on div map-canvas centering around initial
        //latitude and longitude
        var mapOptions = {
            center: initialLatLon,
            zoom: 15
        };
        map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

        //add marker to initial latitude and longitude
        addMarker(initialLatLon);

        //show trucks around that lotion
        showTrucks(initialLon, initialLat);
    };

    // Sets the map on all markers in the array.
    function setAllMap(map) {
        for (var i=0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    function setAllTruckMap(map) {
        for (var i=0; i < truckMarkers.length; i++) {
            var l = truckMarkers[i].length;
            truckMarkers[i][l-1].setMap(map);
        }
    }

    function clearTruckMarkers() {
        setAllTruckMap(null);
    }

    function deleteTruckMarkers() {
        clearTruckMarkers();
        truckMarkers = [];
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setAllMap(null);
    }

    // Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
        clearMarkers();
        markers = [];
    }

    // Shows any markers currently in the array.
    function showMarkers() {
        setAllMap(map);
    }

    // Shows any truck markers currently in the array.
    function showTruckMarkers() {
        setAllTruckMap(map);
    }

    //use Google geocoding API to convert address to lat and lon
    function getLatLonFromAddress(search_text) {
        var api_call = "https://maps.googleapis.com/maps/api/geocode/json?address=" + search_text;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", api_call, false);
        xhr.send();

        var location = JSON.parse(xhr.response)['results'][0];
        var location = location['geometry']['location'];

        return location;
    }

    //Attach handler for enter pressed in search bar event
    $('#searchbox').bind('keypress', function(e) {
        if (e.keyCode==13){

            //clear existing markers on map
            deleteTruckMarkers();
            deleteMarkers();
            event.preventDefault();

            //exchange address for latitude and longitude
            var search_text = $('#searchbox').val();
            var location = getLatLonFromAddress(search_text);
            var latitude = location['lat'];
            var longitude = location['lng'];

            //Set up map center and zoom around new address
            var latlng = new google.maps.LatLng(latitude, longitude);
            addMarker(latlng);
            map.setCenter(latlng);
            map.setZoom(14);

            //Show food trucks around that location
            showTrucks(longitude, latitude);
        }
    });
});